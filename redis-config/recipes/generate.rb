node[:deploy].each do |app_name, deploy_config|
  app_root = "#{deploy_config[:deploy_to]}/current"
  template "#{app_root}/config/redis.yml" do
    source "redis.yml.erb"
    cookbook "redis-config"
    mode "0660"
    group deploy_config[:group]
    owner deploy_config[:user]
    variables( :redis => deploy_config[:redis] || {} )
    not_if do
      deploy_config[:redis].blank?
    end
  end
end
